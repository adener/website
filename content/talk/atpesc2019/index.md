---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "PDE-constrained Optimization Using PETSc/TAO"
event: "Argonne Training Program for Extreme Scale Computing (ATPESC)"
event_url: "https://extremecomputingtraining.anl.gov/archive/atpesc-2019/"
location: "St. Charles, Illinois, USA"
address:
  street:
  city:
  region:
  postcode:
  country:
summary: 
abstract:

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2019-08-06T16:25:00-06:00
date_end: 2019-08-06T17:10:00-06:00
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: 2020-02-19T14:43:14-06:00

authors: [
  "Alp Dener"
]
tags: []

# Is this a featured talk? (true/false)
featured: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

# Optional filename of your slides within your talk's folder or a URL.
url_slides: 

url_code:
url_pdf: https://press3.mcs.anl.gov//atpesc/files/2019/08/ATPESC_2019_Track-5_8_8-6_425pm_Dener-PETSc-TAO.pdf
url_video: https://www.youtube.com/watch?v=co0lGR9cojo&list=PLGj2a3KTwhRYdxtu7uxRvfs26tQKOx3pr&index=8

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---
