---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Investigating Quasi-Newton Outer Product Representations on GPUs"
event: "SIAM Conference on Parallel Processing for Scientific Computing"
event_url: "https://www.siam.org/conferences/cm/conference/pp20"
location: "Seattle, Washington, USA"
address:
  street:
  city:
  region:
  postcode:
  country:
summary:
abstract:

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2020-02-12T14:15:00-08:00
date_end: 2020-02-12T14:35:00-08:00
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: 2020-02-19T14:57:26-06:00

authors: [
  "Alp Dener"
]
tags: []

# Is this a featured talk? (true/false)
featured: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

# Optional filename of your slides within your talk's folder or a URL.
url_slides:

url_code:
url_pdf: files/slides/siam-pp20.pdf
url_video:

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---
