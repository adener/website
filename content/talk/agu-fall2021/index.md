---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Implicit-Explicit and Multirate methods for a Coupled Navier-Stokes Equations"
event: "American Geophysical Union Fall Meeting 2021"
event_url: "https://www.agu.org/Fall-Meeting"
location: "New Orleans, Louisiana, USA"
address:
  street:
  city:
  region:
  postcode:
  country:
summary:
abstract: "Earth system models are composed of coupled components that separately model systems such as the global atmosphere, ocean, and land surface. While these components are well developed, coupling them in a single system can be a signiIcant challenge. Computational efficiency, accuracy, and stability are principal concerns. In this study we focus on these issues. In particular, implicit–explicit (IMEX) and Multirate coupling strategies are explored for handling different time scales. For a simplified model for the air–sea interaction problem, we consider coupled Navier–Stokes equations with an interface condition. Under the rigid-lid assumption, horizontal momentum and heat flux are exchanged through the interface. Several numerical experiments are presented to demonstrate the stability of the coupling schemes."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2021-12-13T16:10:00-08:00
date_end: 2021-12-17T16:30:00-08:00
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: 2021-08-29T14:47:10-06:00

authors: [
  "Shinhoo Kang",
  "Emil M. Constantinescu",
  "Alp Dener",
  "Hong Zhang",
  "Robert L Jacob"
]
tags: []

# Is this a featured talk? (true/false)
featured: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

# Optional filename of your slides within your talk's folder or a URL.
url_slides:

url_code:
url_pdf:
url_video:

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---
