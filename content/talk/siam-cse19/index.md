---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Acelerating Quasi-Newton and Conjugate Gradient Convergence for Large-Scale Optimization"
event: "SIAM Conference on Computational Science and Engineering"
event_url: "https://www.siam.org/conferences/cm/conference/cse19"
location: "Spokane, Washington, USA"
address:
  street:
  city:
  region:
  postcode:
  country:
summary:
abstract:

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2019-02-25T16:10:00-08:00
date_end: 2019-02-25T16:30:00-08:00
all_day: false

# Schedule page publish date (NOT talk date).
publishDate: 2020-02-19T14:47:10-06:00

authors: [
  "Alp Dener",
  "Adam Denchfield",
  "Todd S. Munson"
]
tags: []

# Is this a featured talk? (true/false)
featured: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

# Optional filename of your slides within your talk's folder or a URL.
url_slides:

url_code:
url_pdf: files/slides/siam-cse19.pdf
url_video:

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---
