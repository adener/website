---
# Display name
name: Alp Dener

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Assistant Computational Scientist

# Organizations/Affiliations
organizations:
- name: Argonne National Laboratory
  url: "https://www.anl.gov"

# Short bio (displayed in user profile at end of posts)
bio: Early-career computational scientist specializing on gradient-based and constrained numerical optimization methods for large-scale applications in artificial intelligence, machine learning, simulation-based design and scientific discovery.

interests:
- Simulation-based Optimization
- AI & Machine Learning
- Physics-Informed Neural Nets
- High-Performance Computing

education:
  courses:
  - course: Aeronautical Engineering, Ph.D.
    institution: Rensselaer Polytechnic Institute
    year: 2017
  - course: Mechanical Engineering, B.S.
    institution: University of Maryland, Baltimore County
    year: 2012

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:adener@anl.gov'  # For a direct email link, use "mailto:test@example.org".
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/adener
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=iPoGD0AAAAAJ&hl=en
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/denera/
- icon: cv
  icon_pack: ai
  link: files/cv.pdf

---

I am a computational scientist specializing on gradient-based and constrained numerical optimization methods for large-scale applications in artificial intelligence, machine learning, simulation-based design and scientific discovery. I am also a member of the core development team for [PETSc/TAO](https://petsc.org), one of the premiere numerical libraries in the US Department of Energy research software portfolio. In this capacity, I develop and maintain parallel implementations of optimization algorithms using test-based development and CI/CD workflows on heterogeneous high-performance computing systems.