+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 20  # Order that this section will appear.

title = "Experience"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Assistant Computational Scientist"
  company = "Argonne National Laboratory"
  company_url = "https://www.anl.gov/mcs"
  location = "Lemont, Illinois"
  date_start = "2021-10-01"
  date_end = ""
  description = """
  * Research and develop large-scale optimization methods for engineering design and scientific machine learning.
  * Collaborate with domain experts on challenging applications in plasma physics and Earth systems modeling.
  * Maintain the Toolkit for Advanced Optimization (TAO) package in the PETSc library.
  * Prepare the PETSc library for next-generation exascale supercomputer architectures.
  """

[[experience]]
  title = "Postdoctoral Appointee"
  company = "Argonne National Laboratory"
  company_url = "https://www.anl.gov/mcs"
  location = "Lemont, Illinois"
  date_start = "2018-02-12"
  date_end = "2021-09-30"
  description = """
  * Developed novel stochastic optimization methods for training deep neural networks under physics constraints.
  * Extended conjugate gradient, quasi-Newton and Newton optimization methods in the PETSc/TAO library with active-set bound constraints.
  * Implemented a Python interface for using PETSc/TAO optimization algorithms in PyTorch training workflows.
  * Spearheaded the transition of PETSc/TAO development to a full CI/CD workflow with a Jenkins prototype.
  """

[[experience]]
  title = "Graduate Research and Teaching Assistant"
  company = "Rensselaer Polytechnic Institute"
  company_url = "https://www.optimaldesignlab.com"
  location = "Troy, New York"
  date_start = "2012-08-14"
  date_end = "2017-12-31"
  description = """
  * Researched simulation-driven multidisciplinary design optimization problems.
  * Developed matrix-free optimization library for large-scale engineering design applications.
  * Integrated parallel, high-fidelity, coupled aero-structural solvers with optimization algorithms via adjoint-based sensitivity analysis.
  * Served as a grader, proctor, tutor and substitute lecturer for undergraduate-level mechanical and aeronautical engineering courses.
  """

[[experience]]
  title = "Undergraduate Researcher & Machinist"
  company = "University of Maryland, Baltimore County"
  company_url = "http://jcet.umbc.edu/"
  location = "Baltimore, Maryland"
  date_start = "2010-10-01"
  date_end = "2011-05-30"
  description = """
  * Developed an optical aerosol measurement instrument for deployment on ground and air vehicles.
  * Designed and manufactured micrometer-tolerance aluminum mounts for optical components.
  """

[[experience]]
  title = "Aerodynamic Analysis and Design Intern"
  company = "Turkish Aerospace Industries"
  company_url = "https://www.tusas.com/en"
  location = "Ankara Turkey"
  date_start = "2009-06-01"
  date_end = "2009-09-01"
  description = """
  * Modeled geometry and generated meshes for A129 Mangusta attack helicopter hardpoints.
  * Analyzed effects of rotor downwash on fired ordnance using CFD tools.
  """

+++
