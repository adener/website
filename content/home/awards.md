+++
# Accomplishments widget.
widget = "accomplishments"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 50  # Order that this section will appear.

title = "Awards"
subtitle = ""

# Date format
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Accomplishments.
#   Add/remove as many `[[item]]` blocks below as you like.
#   `title`, `organization` and `date_start` are the required parameters.
#   Leave other parameters empty if not required.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[item]]
  organization = "Argonne National Laboratory"
  organization_url = "https://www.anl.gov"
  title = "Impact Argonne Award, Enhancement of Argonne's Reputation"
  url = ""
  certificate_url = ""
  date_start = "2021-08-18"
  date_end = ""
  description = ""


[[item]]
  organization = "American Institute of Aeronautics and Astronautics AVIATION Forum"
  organization_url = "https://www.aiaa.org/aviation/utility/past-forums"
  title = "MDO Student Paper Competition, 1st Place"
  url = "https://mane.rpi.edu/news/announcements/10182018-1101/students-share-first-place-aiaa-paper-competition"
  certificate_url = ""
  date_start = "2018-06-25"
  date_end = ""
  description = ""

+++
