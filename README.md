Viewing website locally at `localserver:1313`:
```
hugo server
```

Building static website:
```
hugo
```

Static HTML files are under the `public` folder after building. Upload to `public_html` folder on host server.